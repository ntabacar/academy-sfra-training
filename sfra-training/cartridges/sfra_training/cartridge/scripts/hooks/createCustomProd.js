function createProd(data) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');

    Transaction.wrap(function() {
        var newProd = CustomObjectMgr.createCustomObject('AcademyProduct', data.id + '_custom' + Math.floor(Math.random()*100));
        newProd.custom.name = data.name;
        newProd.custom.short_desc = data.desc;
    });
}

exports.createProd = createProd;