function deleteCustomObj(obj) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');

    var objToDelete = CustomObjectMgr.getCustomObject(obj.type, obj.timestamp);

    Transaction.wrap(function() {
        CustomObjectMgr.remove(objToDelete);
    });
}

exports.deleteCustomObj = deleteCustomObj;