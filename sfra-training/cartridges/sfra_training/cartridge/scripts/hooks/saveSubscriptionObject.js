'use strict';

function saveObj(data) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');
    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');

    var currentTimeStamp = StringUtils.formatCalendar(new Calendar(new Date()), "yyyyMMddHHmmssSS");

    Transaction.wrap(function() {
        var newObj = CustomObjectMgr.createCustomObject('NewsletterSubscription2', currentTimeStamp);
        newObj.custom.firstName = data.firstname;
        newObj.custom.lastName = data.lastname;
        newObj.custom.email = data.email;
        newObj.custom.isEmailSent = false;
    });
}

exports.saveObj = saveObj;