'use strict';

/**
 * Fetches the LocalServiceRegistry assigned to a serviceId
 * @param {String} serviceId - service id
 * @returns {Object} a local service registry
 */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

function getServicePost(serviceId) {

    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function(svc, args) {
            // Default request method is post
            // No need to setRequestMethod
            if (args) {
                svc.addHeader('Content-Type', 'application/json');
                svc.addHeader('Accept','application/json');
                svc.addParam('hapikey', '64155f4e-3c4a-49ee-8c86-7b07b622eaa5');
                return JSON.stringify(args);
            } else {
                return null;
            }
        },
        parseResponse: function(svc, client) {
            return client.text;
        },
        mockCall: function(svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }
    });
}

function getServiceGet(serviceId) {

    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function(svc, args) {
            // Default request method is post
            // No need to setRequestMethod
            if (args) {
                if (args.hasOwnProperty('lastIndex')) {
                    svc.addParam('after', args.lastIndex);
                    svc.addParam('limit', '5');
                }
                if (args.hasOwnProperty('contactId')) {
                    svc.URL = svc.URL + '/' + args.contactId;
                }
            }
            else {
                svc.addParam('limit', '5');
            }
            svc.setRequestMethod('GET');
            svc.addHeader('Accept','application/json');
            svc.addParam('hapikey', '64155f4e-3c4a-49ee-8c86-7b07b622eaa5');

            //return JSON.stringify(args);
        },
        parseResponse: function(svc, client) {
            return client.text;
        },
        mockCall: function(svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }
    });
}

function getServiceDelete(serviceId) {

    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function(svc, args) {
            // Default request method is post
            // No need to setRequestMethod
            if (args) { 
                svc.URL = svc.URL + '/' + args.contactId;
                svc.setRequestMethod('DELETE');
                svc.addHeader('Content-Type', 'application/json');
                svc.addParam('hapikey', '64155f4e-3c4a-49ee-8c86-7b07b622eaa5');
            }
        
            
            //return JSON.stringify(args);
        },
        parseResponse: function(svc, client) {
            return client.text;
        },
        mockCall: function(svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }
    });
}

function getServicePatch(serviceId) {

    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function(svc, args) {
            // Default request method is post
            // No need to setRequestMethod
            if (args) {
                svc.setRequestMethod('PATCH');
                svc.URL = svc.URL + '/' + args.contactId;
                svc.addHeader('Content-Type', 'application/json');
                svc.addHeader('Accept','application/json');
                svc.addParam('hapikey', '64155f4e-3c4a-49ee-8c86-7b07b622eaa5');
                return JSON.stringify(args.props);
            } else {
                return null;
            }
        },
        parseResponse: function(svc, client) {
            return client.text;
        },
        mockCall: function(svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }
    });
}

module.exports = {
    getServicePost: getServicePost,
    getServiceGet: getServiceGet,
    getServiceDelete: getServiceDelete,
    getServicePatch: getServicePatch
};
