
var Status = require('dw/system/Status');

var JobTest = function() {
    var args = arguments[0];     
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');
    
    var obj = CustomObjectMgr.getCustomObject(args.TestParam, args.testParam2);
    if (obj != null) {
        Transaction.wrap(function() {
            CustomObjectMgr.remove(obj);
        });
        return new Status(Status.OK);
    } 
    else 
    {
        return new Status(Status.ERROR);
    }
}
            


exports.JobTest = JobTest;