var Status = require('dw/system/Status');

function sendPendingSubscriptions() {
    var Logger = require('dw/system/Logger');
    var customLogger = Logger.getLogger('LoggerTest', 'warn');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
    var HookMgr = require('dw/system/HookMgr');

    var args = arguments[0];

    var allObjs = CustomObjectMgr.queryCustomObjects(args.customObjectType, 'custom.isEmailSent != {0}', 'custom.timestamp ASC', true);

    if ( allObjs.getCount() > 0) {

        var objList = allObjs.asList();
        var reqObject = {};
        var service = newsletterHelpers.getService('crm.newsletter.subscribe');

        for(var i = 0; i < objList.length; i++){
            reqObject = {
                properties: {
                    firstname: objList[i].custom.firstName,
                    lastname: objList[i].custom.lastName,
                    email: objList[i].custom.email
                }
            };

            var response = service.call(reqObject); 

            if (response.ok) {
                var id = JSON.parse(response.object).id;
                customLogger.warn("Subscription with email {0} succesfully processed with id {1}", [reqObject.properties.email, id]);

                obj = {
                    type: args.customObjectType,
                    timestamp: objList[i].custom.timestamp
                };

                HookMgr.callHook('sfra-training.deleteCustomObject', 'deleteCustomObj', obj);
            } else {
                if (response.error === 409) {
                    var errorMessage = JSON.parse(response.errorMessage).message;
                    customLogger.warn(errorMessage);

                    obj = {
                        type: args.customObjectType,
                        timestamp: objList[i].custom.timestamp
                    };

                    HookMgr.callHook('sfra-training.deleteCustomObject', 'deleteCustomObj', obj);
                }
                customLogger.warn("There was an error: " + response.msg);
            }
        }

        return new Status(Status.OK);
    
    } else {
        customLogger.warn("There are no subscriptions pending");
        return new Status(Status.ERROR);
    }    
}

exports.sendPendingSubscriptions = sendPendingSubscriptions;