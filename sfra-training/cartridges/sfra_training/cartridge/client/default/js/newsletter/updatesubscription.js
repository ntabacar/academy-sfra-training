'use strict';

var formValidation = require('base/components/formValidation');

module.exports = {
    newsletterForm: function() {
        $('form.newsletter-form').on('submit', function (e) {
            console.log('got in here hehe');
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr('data-url');
            var id = $form.attr('contact-id');
            console.log(url);
            $form.spinner().start();
            $('form.newsletter-form').trigger('newsletter:submit', e);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: $form.serialize()+'&contactId='+id,
                success: function (data) {
                    console.log($form.serialize()+'&contactId='+id);
                    $form.spinner().stop();
                    if (!data.success) {
                        console.log(data.success);
                        $('.subscription-feedback').text(data.response);
                        $('.subscription-feedback').addClass('failure');
                        $('.newsletter-result').removeClass('hidden');
                        //formValidation($form, data);
                    } else {
                        //window.location.href = data.redirectUrl;
                        $('.subscription-feedback').text(data.response);
                        $('.subscription-feedback').addClass('success');
                        $('.newsletter-result').removeClass('hidden');
                        console.log(data);
                    }
                },
                error: function (err) {
                    console.log($form.serialize()+'&contactId='+id);

                    $form.spinner().stop();
                }
            });
            return false;
        });
    }
};