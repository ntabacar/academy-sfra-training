'use script';

function eventOnDeleteClick() {
    $('.delete-button').on('click', function (e) {
        var $button = $(this);
        e.preventDefault();
        var deleteId = $button.attr('data-target');
        console.log(deleteId);

        var url = './Newsletter-DeleteSubscription';
        console.log(url);
        $button.spinner().start();
        $('.delete-button').trigger('button:click', e);
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {id: deleteId},
            success: function (data) {
                $button.spinner().stop();

                if (!data.success) {
                    console.log('there was an error');
                } else {
                    //window.location.href = data.redirectUrl;
                    console.log('item deleted');
                    var parent = $button.closest('.product-info');
                    parent.remove();
                    var number = parseInt($('.number-of-items').text());
                    $('.number-of-items').text(number - 1);
                }
            },
            error: function (err) {
                $button.spinner().stop();

                console.log('error!');
            }
        });
        return false;
    });
}
/*
function onItemClick() {
    $('.edit-button').on('click', function (e) {
        console.log('item clicked');
        var $element = $(this);
        e.preventDefault();

        var parent = $element.closest('.product-info');
        var url = './Newsletter-Edit';
        var _id = $element.attr('.data-target');
        var _firstname = parent.find('.fname-value').text();
        var _lastname = parent.find('.lname-value').text();
        var _email = parent.find('.email-value').text();

        var reqObject = {
            id: _id,
            firtname: _firstname,
            lastname: _lastname,
            email: _email
        };

        console.log(url);
        $element.spinner().start();
        $('.edit-button').trigger('button:click', e);
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: reqObject,
            success: function (data) {
                $element.spinner().stop();
                console.log('ok');
                //window.location.href = data.redirectUrl;
                window.location.replace(data.redirectUrl);
            },
            error: function (data) {
                $element.spinner().stop();
                console.log('failed');
            }
        });
        return false;
    });
}*/

module.exports = {
    expandList: function() {
        $('#listmore').on('click', function (e) {
            console.log('got in here hehe');
            var $button = $(this);
            e.preventDefault();

            var url = $button.attr('data-url');
            var lastId = $button.attr('last-index');
            var numOfItems = $('.number-of-items').text();
            var reqbody = {
                numberOfItems: numOfItems,
                lastIndex: lastId
            };
            console.log(url);
            $button.spinner().start();
            $('#listmore').trigger('button:click', e);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: reqbody,
                success: function (data) {
                    $button.spinner().stop();

                    if (!data.success) {
                        console.log(data.success);
                        $('#newresults').text(data.response);
                    } else {
                        $.each(data.response, function(index) {
                            var url_edit = './Newsletter-Edit';
                            $('#newresults').append( `<div class="card product-info">
                                                        <div class="line-item-header">
                                                            <div class="line-item-name">
                                                                <span class="return-id"> ` + data.response[index].id +  `</span>
                                                            </div>
                                                            <div class="remove-line-item">
                                                                <button type="button" name="delete-button" class="remove-btn-lg remove-product btn btn-light delete-button" data-target="`+data.response[index].id+`"><span>x</span></button>
                                                                <a href="Newsletter-Edit?contactId=`+data.response[index].id+`"><span>Edit</span></a>
                                                            </div>
                                                        </div> 
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="item-attributes">
                                                                    <p class="line-item-attributes">First name:  <span class="fname-value">` + data.response[index].firstname + `</span> </p>
                                                                    <p class="line-item-attributes">Last name: <span class="lname-value"> ` + data.response[index].lastname +  `</span> </p>
                                                                    <p class="line-item-attributes">Email:  <span class="email-value">`+ data.response[index].email +  `</span> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`);
                        });
                        $button.attr('last-index', data.lastIndex);
                        $('.number-of-items').text(data.numberOfItems);
                        console.log(data);
                        $('.delete-button').off('click');
                        $('product-info').off('click');
                        eventOnDeleteClick();
                    }
                },
                error: function (err) {
                    $button.spinner().stop();

                    console.log('error!');
                }
            });
            return false;
        });
    },

    deleteSub: function() {
        eventOnDeleteClick();
    }
};

