'use strict'

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');
var ProductMgr = require('dw/catalog/ProductMgr');
var HookMgr = require('dw/system/HookMgr');
var Logger = require('dw/system/Logger');


server.get('Show',
            server.middleware.https,
            function (req, res, next){
                var customPrefValue = Site.getCurrent().getCustomPreferenceValue("isAcademyProductActive");

                var customLogger = Logger.getLogger('LoggerTest', 'warn');
                customLogger.warn('This works!');

                if(customPrefValue === false) {
                    res.render('academyproducterror');
                } else {
                    var product = ProductMgr.getProduct('25686364M');

                    var JSONRes = HookMgr.callHook('sfra-training.productHook', 'productDetails', product);


                    res.render('academyproduct', JSONRes);

                }
                next();
            });

module.exports = server.exports();