'use strict';

//Use the following for CSRF protection: add middleware in routes and hidden field on form
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var HookMgr = require('dw/system/HookMgr');


server.get(
    'Show',
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: dw.web.URLUtils.url('Newsletter-Create'),
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.get(
    'Edit',
    server.middleware.https,
    function (req, res, next) {
        
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var service = newsletterHelpers.getServiceGet('crm.newsletter.getlist');

        var contact = req.querystring.contactId;
        var response = service.call({contactId: contact});


        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();
        
        if (response.ok) {
            var jsonResponse = [];
            var object = JSON.parse(response.object);

            newsletterForm.email.htmlValue = object.properties.email;
            newsletterForm.fname.htmlValue = object.properties.firstname;
            newsletterForm.lname.htmlValue = object.properties.lastname;
        }


        
/*
        res.json({
            redirectUrl: dw.web.URLUtils.url('newsletter/newsletteredit.isml').toString(),
            newsletterForm: newsletterForm,
            actionUrl: dw.web.URLUtils.url('Newsletter-Update').toString()
        })
*/
 
        res.render('newsletter/newsletteredit', {
            actionUrl: dw.web.URLUtils.url('Newsletter-Update'),
            newsletterForm: newsletterForm,
            id: contact
        });

        next();
    }
)

server.post(
    'Update',
    server.middleware.https,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');

        var email = newsletterForm.email.htmlValue;
        var firstname = newsletterForm.fname.htmlValue;
        var lastname = newsletterForm.lname.htmlValue;

        var service = newsletterHelpers.getServicePatch('crm.newsletter.subscribe');
        var reqObject = {
            properties : {
            firstname: firstname,
            lastname: lastname, 
            email: email
            }
        };

        var id = req.form.contactId;
        var response = service.call({props: reqObject, contactId: id});

        var jsonResponse = {};

        if (response.ok) {
            jsonResponse = {
                success: true,
                firstname: firstname,
                lastname: lastname,
                email: email,
                redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString(),
                response: response.msg
            };
        } else {
            jsonResponse = {
                response: 'There was an error: ' + response.msg,
                success: false
            };
        }

        res.json(jsonResponse);

        next();
    }
)

server.post('Create',
    server.middleware.https,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');

        var email = newsletterForm.email.htmlValue;
        var firstname = newsletterForm.fname.htmlValue;
        var lastname = newsletterForm.lname.htmlValue;

        var service = newsletterHelpers.getServicePost('crm.newsletter.subscribe');
        var reqObject = {
            properties : {
            firstname: firstname,
            lastname: lastname,
            email: email
            }
        };

        var response = service.call(reqObject); 
        /*var response = {
            ok: false,
            msg: "service unavailable"
        };*/

        var jsonResponse = {};

        if (response.ok) {
            jsonResponse = {
                success: true,
                firstname: firstname,
                lastname: lastname,
                email: email,
                redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString(),
                response: response.msg
            };
        } else {
            jsonResponse = {
                response: 'There was an error: ' + response.msg,
                success: false
            };

            HookMgr.callHook('sfra-training.saveSubscriptionObject', 'saveObj', reqObject.properties);
        }

        res.json(jsonResponse);

        next();
    }
);

server.get(
    'AddResults',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var service = newsletterHelpers.getServiceGet('crm.newsletter.getlist');
        var response = service.call({ lastIndex: req.querystring.lastIndex });

        if (response.ok) {
            var jsonResponse = [];
            var object = JSON.parse(response.object);
            var results = object.results;

            for (var i = 0; i < results.length; i++) {
                var obj = {};
                obj.id = results[i].id;
                obj.firstname = results[i].properties.firstname;
                obj.lastname = results[i].properties.lastname;
                obj.email = results[i].properties.email;

                jsonResponse.push(obj);
            }

            res.json({
                actionUrl: dw.web.URLUtils.url('Newsletter-AddResults'),
                success: true,
                numberOfItems: results.length + parseInt(req.querystring.numberOfItems),
                lastIndex: results[results.length - 1].id,
                response: jsonResponse
            });
        }
        next();
    }
);

server.get(
    'AllSubscriptions',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var service = newsletterHelpers.getServiceGet('crm.newsletter.getlist');

        var response = service.call();

        if (response.ok) {
            var jsonResponse = [];
            var object = JSON.parse(response.object);
            var results = object.results;

            for (var i = 0; i < results.length; i++) {
                var obj = {};
                obj.id = results[i].id;
                obj.firstname = results[i].properties.firstname;
                obj.lastname = results[i].properties.lastname;
                obj.email = results[i].properties.email;

                jsonResponse.push(obj);
            }

            res.render('/newsletter/newsletterlist', {
                actionUrl: dw.web.URLUtils.url('Newsletter-AddResults'),
                success: true,
                numberOfItems: results.length,
                lastIndex: results[results.length - 1].id,
                response: jsonResponse
            });
        }
        else {
            res.json({
                success: false
            });
        }
        next();
    }
);

server.get(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/newslettersuccess', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')
        });

        next();
    }
);

server.post(
    'Handler',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var continueUrl = dw.web.URLUtils.url('Newsletter-Show');

        // Perform any server-side validation before this point, and invalidate form accordingly
        if (newsletterForm.valid) {
            // Send back a success status, and a redirect to another route
            res.json({
                success: true,
                redirectUrl: URLUtils.url('Newsletter-Success').toString()
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Error-Start').toString()
            });
        }
        next();
    }
);

server.post(
    'DeleteSubscription',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var service = newsletterHelpers.getServiceDelete('crm.newsletter.getlist');

        var response = service.call({ contactId: req.form.id });

        if (response.ok) {
            res.json({
                success: true
            });
        } else {
            res.json({
                success: false
            });
        }
        next();
    }
);

module.exports = server.exports();