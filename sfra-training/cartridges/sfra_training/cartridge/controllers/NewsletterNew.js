'use strict';

//Use the following for CSRF protection: add middleware in routes and hidden field on form

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');
var StringUtils = require('dw/util/StringUtils');
var HookMgr = require('dw/system/HookMgr');
var PaymentMgr = require('dw/order/PaymentMgr');

var customObjName = HookMgr.callHook('sfra-training.customPref', 'prefValue', 'newsletter-sub');

server.get('Create',
            server.middleware.https,
            function (req, res, next) {
                
                var name = req.querystring.name;
                var surname = req.querystring.surname;
                var email = req.querystring.email;
                var currentTimeStamp = StringUtils.formatCalendar(new Calendar(new Date()), "yyyyMMddHHmmssSS");

                if (email != null) {
                    Transaction.wrap(function() {
                        var newObj = CustomObjectMgr.createCustomObject(customObjName, currentTimeStamp);
                        newObj.custom.firstName = name;
                        newObj.custom.lastName = surname;
                        newObj.custom.email = email;
                    });
    
                    res.json({
                        success: true,
                        'name': name,
                        'surname': surname,
                        'email': email
                    });
    
                }
                else {
                    res.json({
                        error: true
                    });
                }
                
                next();
        });

server.get('Get',
            server.middleware.https,
            function(req, res, next) {

                var param = req.querystring.param;

                if (!param) {
                    var allObjs = CustomObjectMgr.getAllCustomObjects(customObjName);
                    var objList = allObjs.asList();

                    var JSONRes = [];

                    for(var i = 0; i< objList.length; i++) {
                        var obj = {};
                        obj.timestamp = objList[i].custom.timestamp;
                        obj.firstName = objList[i].custom.firstName;
                        obj.lastName = objList[i].custom.lastName;
                        obj.email = objList[i].custom.email;

                        JSONRes.push(obj);
                    };     

                    res.json({
                        success: true,
                        'objects': JSONRes
                    });
                } else {
                    var obj = CustomObjectMgr.getCustomObject(customObjName, param);

                    if(obj != null) {

                        var JSONRes = [];
                        JSONRes.push({
                            'name': obj.custom.firstName,
                            'surname': obj.custom.lastName,
                            'email': obj.custom.email
                        });

                        res.json({
                            success: true,
                            'obj':JSONRes
                        });
                    }

                    else {
                        res.json({
                            error: true,
                            'message': 'key doesn\'t exist'
                        });
                    }
                }
                
                next();
            });

    server.get('Delete',
                server.middleware.https,
                function(req, res, next) {
                    var param = req.querystring.param;

                    if (param != null) {

                        Transaction.wrap( function(){
                            var obj = CustomObjectMgr.getCustomObject(customObjName, param);
                            if(obj) {
                                CustomObjectMgr.remove(obj);
                                res.json({
                                    success: true
                                });
                            }
                            else {
                                res.json({
                                    error: true,
                                    'message': 'object doesn\'t exist'
                                });
                            }   
                        });

                    }
                    else {
                        res.json({
                            error: true,
                            'message': 'missing id'
                        });
                    }  
                    next();          
                });
                
    
    server.get('Payment',
                server.middleware.https,
                function(req, res, next) {

                    var paymentMethods = PaymentMgr.getActivePaymentMethods();

                    if(paymentMethods) {

                        var JSONRes = [];

                        for(var i = 0; i<paymentMethods.length; i++) {
                            var obj = {};
                            obj.name = paymentMethods[i].getName();
                            obj.id = paymentMethods[i].getID();
                            obj.description = paymentMethods[i].getDescription();
                            obj.isActive = paymentMethods[i].isActive();

                            var cards = paymentMethods[i].getActivePaymentCards();

                            if (cards) {

                                obj.cards = [];

                                for(var y = 0; y<cards.length; y++){
                                    var card = {};
                                    card.type = cards[y].getCardType();
                                    card.name = cards[y].getName();
                                    card.description = cards[y].getDescription();
    
                                    obj.cards.push(card);
                                }
                            }
                            

                            JSONRes.push(obj);
                        }

                        res.json({
                            success: true,
                            "methods": JSONRes
                        });
                    }
                    else {
                        res.json({
                            success: false,
                            "error": "no active payment methods"
                        });
                    }
                    next();
                });


module.exports = server.exports();