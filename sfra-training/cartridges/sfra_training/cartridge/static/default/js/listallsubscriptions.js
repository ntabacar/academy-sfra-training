/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (include) {
    if (typeof include === 'function') {
        include();
    } else if (typeof include === 'object') {
        Object.keys(include).forEach(function (key) {
            if (typeof include[key] === 'function') {
                include[key]();
            }
        });
    }
};


/***/ }),
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var processInclude = __webpack_require__(1);

$(document).ready(function () {
    processInclude(__webpack_require__(4));
});

/***/ }),
/* 4 */
/***/ (function(module, exports) {

'use script';

function eventOnDeleteClick() {
    $('.delete-button').on('click', function (e) {
        var $button = $(this);
        e.preventDefault();
        var deleteId = $button.attr('data-target');
        console.log(deleteId);

        var url = './Newsletter-DeleteSubscription';
        console.log(url);
        $button.spinner().start();
        $('.delete-button').trigger('button:click', e);
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {id: deleteId},
            success: function (data) {
                $button.spinner().stop();

                if (!data.success) {
                    console.log('there was an error');
                } else {
                    //window.location.href = data.redirectUrl;
                    console.log('item deleted');
                    var parent = $button.closest('.product-info');
                    parent.remove();
                    var number = parseInt($('.number-of-items').text());
                    $('.number-of-items').text(number - 1);
                }
            },
            error: function (err) {
                $button.spinner().stop();

                console.log('error!');
            }
        });
        return false;
    });
}
/*
function onItemClick() {
    $('.edit-button').on('click', function (e) {
        console.log('item clicked');
        var $element = $(this);
        e.preventDefault();

        var parent = $element.closest('.product-info');
        var url = './Newsletter-Edit';
        var _id = $element.attr('.data-target');
        var _firstname = parent.find('.fname-value').text();
        var _lastname = parent.find('.lname-value').text();
        var _email = parent.find('.email-value').text();

        var reqObject = {
            id: _id,
            firtname: _firstname,
            lastname: _lastname,
            email: _email
        };

        console.log(url);
        $element.spinner().start();
        $('.edit-button').trigger('button:click', e);
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: reqObject,
            success: function (data) {
                $element.spinner().stop();
                console.log('ok');
                //window.location.href = data.redirectUrl;
                window.location.replace(data.redirectUrl);
            },
            error: function (data) {
                $element.spinner().stop();
                console.log('failed');
            }
        });
        return false;
    });
}*/

module.exports = {
    expandList: function() {
        $('#listmore').on('click', function (e) {
            console.log('got in here hehe');
            var $button = $(this);
            e.preventDefault();

            var url = $button.attr('data-url');
            var lastId = $button.attr('last-index');
            var numOfItems = $('.number-of-items').text();
            var reqbody = {
                numberOfItems: numOfItems,
                lastIndex: lastId
            };
            console.log(url);
            $button.spinner().start();
            $('#listmore').trigger('button:click', e);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: reqbody,
                success: function (data) {
                    $button.spinner().stop();

                    if (!data.success) {
                        console.log(data.success);
                        $('#newresults').text(data.response);
                    } else {
                        $.each(data.response, function(index) {
                            var url_edit = './Newsletter-Edit';
                            $('#newresults').append( `<div class="card product-info">
                                                        <div class="line-item-header">
                                                            <div class="line-item-name">
                                                                <span class="return-id"> ` + data.response[index].id +  `</span>
                                                            </div>
                                                            <div class="remove-line-item">
                                                                <button type="button" name="delete-button" class="remove-btn-lg remove-product btn btn-light delete-button" data-target="`+data.response[index].id+`"><span>x</span></button>
                                                                <a href="Newsletter-Edit?contactId=`+data.response[index].id+`"><span>Edit</span></a>
                                                            </div>
                                                        </div> 
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="item-attributes">
                                                                    <p class="line-item-attributes">First name:  <span class="fname-value">` + data.response[index].firstname + `</span> </p>
                                                                    <p class="line-item-attributes">Last name: <span class="lname-value"> ` + data.response[index].lastname +  `</span> </p>
                                                                    <p class="line-item-attributes">Email:  <span class="email-value">`+ data.response[index].email +  `</span> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`);
                        });
                        $button.attr('last-index', data.lastIndex);
                        $('.number-of-items').text(data.numberOfItems);
                        console.log(data);
                        $('.delete-button').off('click');
                        $('product-info').off('click');
                        eventOnDeleteClick();
                    }
                },
                error: function (err) {
                    $button.spinner().stop();

                    console.log('error!');
                }
            });
            return false;
        });
    },

    deleteSub: function() {
        eventOnDeleteClick();
    }
};



/***/ })
/******/ ]);