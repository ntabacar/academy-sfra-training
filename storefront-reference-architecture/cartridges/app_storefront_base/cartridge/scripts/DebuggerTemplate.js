'use strict';

function debug(){
    return true;
}

module.exports.debug = debug;

/*

<isscript>
var debug = require('app_storefront_base/cartridge/scripts/DebuggerTemplate').debug();
var fine = 'fine';
</isscript>

*/